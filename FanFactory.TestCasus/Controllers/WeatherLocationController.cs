﻿using FanFactory.TestCasus.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace FanFactory.TestCasus.Controllers
{
    public class WeatherLocationController : ApiController
    {
        private readonly string url = "https://data.buienradar.nl/1.0/feed/xml";
        // GET api/<controller>/5
        public IHttpActionResult Get(string location)
        {
            var stationNodes = XDocument
                .Load(url)
                .Descendants("weerstations")
                .First();

            var serializer = new XmlSerializer(typeof(Weerstations));

            using (var reader = stationNodes.CreateReader())
            {
                var stations = (Weerstations)serializer.Deserialize(reader);
                var station = stations.WeerstationList.FirstOrDefault(o => o.StationNaam.Regio.ToLower() == location.ToLower());

                if (station == null)
                    return NotFound();

                return Ok(station);
            }
        }
    }
}