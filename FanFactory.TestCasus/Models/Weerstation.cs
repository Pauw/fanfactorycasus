﻿using System;
using System.Xml.Serialization;

namespace FanFactory.TestCasus.Models
{

    public class Weerstation
    {
        [XmlElement(ElementName = "stationcode")]
        public int StationCode { get; set; }
        [XmlElement(ElementName = "stationnaam")]
        public StationNaam StationNaam{ get; set; }


        [XmlElement(ElementName = "temperatuurGC")]
        public string TempGc { get; set; }

        [XmlElement(ElementName = "temperatuur10cm")]
        public string Temp10cm { get; set; }
    }

    public class StationNaam
    {
        [XmlText]
        public string Value { get; set; }
        [XmlAttribute(AttributeName = "regio")]
        public string Regio { get; set; }

    }
}
