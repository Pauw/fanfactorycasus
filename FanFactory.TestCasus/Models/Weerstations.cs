﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FanFactory.TestCasus.Models
{
    [XmlRoot("weerstations")]
    public class Weerstations
    {
        [XmlElement(ElementName = "weerstation")]
        public List<Weerstation> WeerstationList { get; set; }
    }
}